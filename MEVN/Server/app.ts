import express from "express";
import morgan from "morgan";
import cors from "cors";
import rutasCategoria from "./routes/rutasCat";
import rutasClientes from "./routes/rutasCli";
import rutasProductos from "./routes/rutasProd";

const app = express();

app.use(morgan("dev"));
app.use(cors());
app.use(express.json());

app.use("/", rutasCategoria);
app.use("/",rutasClientes);
app.use("/", rutasProductos);

export default app;
