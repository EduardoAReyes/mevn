import { Router } from "express";
import controlador from "../controllers/controladoresProd";
const rutasProductos = Router();

rutasProductos.get("/consultaProd",controlador.consultarProductos);

export default rutasProductos;
