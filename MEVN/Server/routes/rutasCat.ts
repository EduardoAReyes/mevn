import { Router } from "express";
import controlador from "../controllers/controladoresCat";

const rutasCategoria = Router();

rutasCategoria.get("/consultaCat",controlador.consultarCategoria);
rutasCategoria.post("/ingresaCat", controlador.ingresarCategoria);
rutasCategoria.put("/actualizaCat",controlador.actualizarCategoria);
rutasCategoria.delete("/eliminaCat",controlador.eliminarCategoria);

export default rutasCategoria;
