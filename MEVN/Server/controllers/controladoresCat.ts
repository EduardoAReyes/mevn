import { Request, Response } from "express";
import { MongoClient } from "mongodb";

type controladorType = { [key: string]: (req: Request, res: Response) => void };

const controlador: controladorType = {};

const url =
  "mongodb://myUserAdmin:hola1234@127.0.0.1:27017/?authSource=admin&readPreference=primary&ssl=false&directConnection=true";

const client = new MongoClient(url);

const dbName = "Tienda_en_linea";

const db = client.db(dbName);

const collection = db.collection("Categoria");

controlador.consultarCategoria = async (req, res) => {
  try {
    await client.connect();
    const consultar = await collection.find({}).toArray();

    res.send(consultar);
  } catch (error) {
    console.error(error);
  } finally {
    await client.close();
  }
};

controlador.ingresarCategoria = async (req, res) => {
  try {
    await client.connect();

    const doc = {
      title: "Random Harvest",
      content: "No bytes, no problem. Just insert a document, in MongoDB",
    };
    const ingresar = collection.insertOne(doc);

    res.send((await ingresar).insertedId);
  } catch (error) {
    console.error(error);
  } finally {
    await client.close();
  }
};

controlador.actualizarCategoria = async (req, res) => {
  try {
    await client.connect();

    const filter = { title: "Random Harvest" };

    const options = { upsert: true };

    const updateDoc = {
      $set: {
        descripcion: `A harvest of random numbers, such as: ${Math.random()}`,
      },
    };
    const result = await collection.updateOne(filter, updateDoc, options);

    res.send(result);
  } catch (error) {
    console.error(error);
  } finally {
    await client.close();
  }
};

controlador.eliminarCategoria = async (req, res) => {
  try {
    await client.connect();

    const query = { title: "Record of a Shriveled Datum" };
    const result = await collection.deleteOne(query);
    if (result.deletedCount === 1) {
      console.log("Successfully deleted one document.");
      res.send(result);
    } else {
      console.log("No documents matched the query. Deleted 0 documents.");
      res.send("Nada");
    }
  } catch (error) {
    console.error(error);
  } finally {
    await client.close();
  }
};

export default controlador;
