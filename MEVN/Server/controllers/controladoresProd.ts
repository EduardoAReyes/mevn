import { Request, Response } from "express";
type controladorType = { [key: string]: (req: Request, res: Response) => void };

const controlador: controladorType = {};

controlador.consultarProductos = async (req, res) => {
  res.send("hola prod");
};

export default controlador;
